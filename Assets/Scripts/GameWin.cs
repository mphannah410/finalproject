using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameWin : MonoBehaviour
{

    public GameObject gameWinText;

    // Start is called before the first frame update
    void Start()
    {
        gameWinText.GetComponent<Text>().enabled = false;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        gameWinText.GetComponent<Text>().enabled = true;
    }
}
