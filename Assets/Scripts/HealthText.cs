using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthText : MonoBehaviour
{

    public GameObject Player; 
    public Text healthText;
    private int initHealth;

    // Start is called before the first frame update
    void Start()
    {
        initHealth = Player.GetComponent<PlayerCombat>().health;
        healthText.text = "Health: " + initHealth.ToString(); 
    }

    // Update is called once per frame
    public void Hit(int health)
    {
        healthText.text = "Health: " + health.ToString(); 
    }
}
